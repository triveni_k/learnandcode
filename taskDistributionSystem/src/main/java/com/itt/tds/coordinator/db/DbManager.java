package com.itt.tds.coordinator.db;
import com.itt.tds.cfg.TDSConfiguration;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public class DbManager {
	private static DbManager dbManager;

	public static DbManager getInstance() {
		if (dbManager == null) {
			dbManager = new DbManager();
		}
		return dbManager;		
	}

	public Connection getConnection() throws SQLException, IOException {
		TDSConfiguration tdsConfig = TDSConfiguration.getInstance();
		Connection connection=DriverManager.getConnection(tdsConfig.getDBConnectionString());
		return connection;
	}
	public void closeConnection(Connection conn) throws SQLException {
		conn.close();
		dbManager  = null;
	}

}