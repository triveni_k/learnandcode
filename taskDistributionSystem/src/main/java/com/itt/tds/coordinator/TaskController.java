package com.itt.tds.coordinator;

import java.io.IOException;
import java.sql.SQLException;

import com.itt.tds.client.Client;
import com.itt.tds.comm.SocketHandler;
import com.itt.tds.comm.TDSRequest;
import com.itt.tds.comm.TDSResponse;
import com.itt.tds.coordinator.db.repository.ClientRepositoryImplementation;
import com.itt.tds.coordinator.db.repository.TaskRepositoryImpl;
import com.itt.tds.core.Task;

public class TaskController  implements TDSController {

	@Override
	public TDSResponse processRequest(TDSRequest request) throws IOException, SQLException {
		// TODO Auto-generated method stub
		if(request.getMethod().equals("task-add")){
			addTask(request);
			return new TDSResponse();
		}else if(request.getMethod().equals("task-modify")){
			return modifyNode(request);
		}else if(request.getMethod().equals("task-get")){
			return fetchResults(request.getParameters("taskId")); 
		}else {
			return deleteNode(request);
		}
	}

	private TDSResponse fetchResults(String taskId) throws IOException, SQLException {
		TaskRepositoryImpl taskrepo = new TaskRepositoryImpl();
		Task task =  taskrepo.getTaskById(Integer.parseInt(taskId)).get(0);
		System.out.println("@@@@@@@@@@@@@" + task);
		TDSResponse res = new TDSResponse();
		res.addParameter("taskStatus", task.getTaskName());
		return res;
	}

	private TDSResponse deleteNode(TDSRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	private TDSResponse modifyNode(TDSRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	private void addTask(TDSRequest request) throws IOException, SQLException {
		TaskRepositoryImpl taskrepo = new TaskRepositoryImpl();
		ClientRepositoryImplementation clientImpl = new ClientRepositoryImplementation();
		int clientId = clientImpl.getClientIdByHostName(request.getSourceIp());
		if(clientId<0) {
			Client client = new Client();
			client.setHostName(request.getSourceIp());
			client.setUserName(request.getParameters("userName"));
			clientImpl.Add(client);
			clientId = clientImpl.getClientIdByHostName(request.getSourceIp());
		}
		Task taskInstance = new Task();
		taskInstance.setTaskName("sample");
		taskInstance.setTaskState(1);
		taskInstance.setTaskExePath("samplePath");
		taskInstance.setUserId(clientId);
		taskInstance.setProgramBytes(request.getData());
		taskrepo.Add(taskInstance);
		new SocketHandler().setTask(taskInstance);
		// TODO Auto-generated method stub

	}

}
