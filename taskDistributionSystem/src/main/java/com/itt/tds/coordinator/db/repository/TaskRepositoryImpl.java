package com.itt.tds.coordinator.db.repository;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.itt.tds.coordinator.Node;
import com.itt.tds.coordinator.db.DbManager;
import com.itt.tds.core.Task;
import com.itt.tds.core.TaskStatus;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set; 

public class TaskRepositoryImpl implements TaskRepositroy {
	Properties properties = new Properties();
	public TaskRepositoryImpl() throws IOException{
		FileInputStream input = new FileInputStream("src//main//java//com//itt//tds//cfg//config.properties");
    	properties.load(input);
	}
	public int Add(Task taskInstance) throws SQLException, IOException {
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try {
			String query = "INSERT INTO Task (taskName,taskPath,userID,taskState,task)VALUES(?,?,?,?,?)";	
			PreparedStatement pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, taskInstance.getTaskName());
			pstmt.setString(2, taskInstance.getTaskExePath());
			pstmt.setInt(3, taskInstance.getUserId());	
			pstmt.setInt(4, taskInstance.getTaskState());
			pstmt.setString(5, taskInstance.getProgramBytes().toString());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			int recordId = 0;
			if(rs.next())
			{
				recordId = rs.getInt(1);
			}
			
			return recordId;
		}finally {
			dbManager.closeConnection(conn);
		}
	}

	public int Modify(Task taskInstance) throws SQLException, IOException{
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try {
			String query = properties.getProperty("comm.task.query.update");
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, taskInstance.getTaskState());
			preparedStmt.setInt(2, taskInstance.getId());
			return preparedStmt.executeUpdate();
		}finally {
			dbManager.closeConnection(conn);
		}
	}
	public List<Task> GetTasksByClientId(int clientId) throws SQLException, IOException{
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		List<Task> tasks = new ArrayList<Task>();
		String query = "select * from Task where userID = " + clientId + ";" ;
		Statement statement=conn.createStatement();
		ResultSet rs = statement.executeQuery(query);	
		createTaskList(tasks, rs);
		return tasks;
	}
	private void createTaskList(List<Task> tasks, ResultSet rs) throws SQLException {
		while (rs.next()) {  
			Task task = new Task();
			task.setTaskName(rs.getString("taskName"));
			task.setTaskExePath(rs.getString("taskPath"));
			task.setUserId(rs.getInt("userID"));
			task.setAssignedNodeId(rs.getInt("assignedNodeId"));
			task.setId(rs.getInt("taskId"));
			tasks.add(task);
		}
	}
	public int SetTaskStatus(int taskId, int status) throws SQLException, IOException {
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try {
			String query = properties.getProperty("comm.task.query.setTaskStatus");
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, status);
			preparedStmt.setInt(2, taskId);
			return preparedStmt.executeUpdate();
		}
		finally {
			dbManager.closeConnection(conn);
		}
	}
	public List<Task> GetTasksByNodeId(int nodeId) throws SQLException, IOException{
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		List<Task> tasks = new ArrayList<Task>();
			String query = "select * from Task where assignedNodeId = " + nodeId;
			Statement statement=conn.createStatement();
			ResultSet rs = statement.executeQuery(query);		                         
			createTaskList(tasks, rs);
			dbManager.closeConnection(conn);
			return  tasks;
			}

		public void Delete(int taskId) throws SQLException, IOException{
			DbManager dbobj = DbManager.getInstance();
			Connection conn = dbobj.getConnection();
			try {
					String query = properties.getProperty("comm.task.query.delete");
					PreparedStatement preparedStmt = conn.prepareStatement(query);
					preparedStmt.setInt(1, taskId);
					preparedStmt.execute();
				}
				finally {
				dbobj.closeConnection(conn);
			}
		}


	

	public int AssignNode(Node node, int taskId) throws SQLException, IOException {
		DbManager dbobj = DbManager.getInstance();
		Connection conn = dbobj.getConnection();
		try {
			String query = properties.getProperty("comm.task.query.AssignNode");
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, node.getNodeId());
			preparedStmt.setInt(2, taskId);
			return preparedStmt.executeUpdate();
		} finally {
			dbobj.closeConnection(conn);
		}
	}
	public List<Task> getTaskById(int i) throws SQLException, IOException {
		// TODO Auto-generated method stub
		DbManager dbobj = DbManager.getInstance();
		Connection conn = dbobj.getConnection();
		String query = "select * from Task where id = " + i;
		Statement statement=conn.createStatement();
		List<Task> tasks = new ArrayList<Task>();
		ResultSet rs = statement.executeQuery(query);	
		createTaskList(tasks, rs);
		return tasks;
	}




}
