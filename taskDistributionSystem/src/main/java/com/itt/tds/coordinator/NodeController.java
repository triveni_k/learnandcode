package com.itt.tds.coordinator;


import com.itt.tds.comm.TDSRequest;
import com.itt.tds.comm.TDSResponse;
import com.itt.tds.coordinator.db.DbManager;
import com.itt.tds.coordinator.db.repository.NodeRepositoryImplementation;
import com.itt.tds.core.TaskResult;

import java.io.IOException;
import java.sql.SQLException;

/**
 * 
 */
public class NodeController implements TDSController {

    /**
     * Default constructor
     */
    public NodeController() {

    }
    public TDSResponse processRequest(TDSRequest request) throws IOException, SQLException {
        if(request.getMethod().equals("node-add")){
            	addNode(request);
            	return new TDSResponse();
        }else if(request.getMethod().equals("node-modify")){
            return modifyNode(request);
        }else {
            return deleteNode(request);
        }
    }

    private TDSResponse deleteNode(TDSRequest request) throws IOException, SQLException {
    	System.out.println("node-delete");
		return null;
    }

    private TDSResponse modifyNode(TDSRequest request) throws IOException, SQLException{
    	System.out.println("node-modify");
		return null;   
    }

    private void addNode(TDSRequest request) throws IOException, SQLException {
    	System.out.println("node-add");
    }

    /**
     * 
     */
    public void run() {
        // TODO implement here
    }

    /**
     * @return
     */
    private NodeRequest waitForNode() {
        // TODO implement here
        return null;
    }

    /**
     * @param node
     */
    private void registerNode(Node node) {
        // TODO implement here
    }

    /**
     * @param taskResult
     */
    private void saveResult(TaskResult taskResult) {
        // TODO implement here
    }

}