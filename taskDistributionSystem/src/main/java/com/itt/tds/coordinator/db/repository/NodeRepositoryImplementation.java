package com.itt.tds.coordinator.db.repository;

import com.itt.tds.coordinator.Node;
import com.itt.tds.coordinator.db.DbManager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;



public class NodeRepositoryImplementation implements NodeRepository{
	Properties properties = new Properties();
	public NodeRepositoryImplementation() throws IOException{
		FileInputStream input = new FileInputStream("src//main//java//com//itt//tds//cfg//config.properties");
    	properties.load(input);
	}


	public int Add(Node node) throws SQLException, IOException  {
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try {
		String query = properties.getProperty("comm.node.query.add");
		PreparedStatement pstmt = conn.prepareStatement(query);
		pstmt.setString(1, node.getNodeIp());
		pstmt.setInt(2, node.getNodePort());
		pstmt.setInt(3, node.getNodeStatus());
		 return pstmt.executeUpdate();
		}
		finally {
			dbManager.closeConnection(conn);
		}
	}

	public int Modify(Node node) throws SQLException, IOException {
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try {
			String query = properties.getProperty("comm.node.query.modify");
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, node.getNodeStatus());
			pstmt.setInt(2, node.getNodeId());
			return pstmt.executeUpdate();
		}
		finally
		{
			dbManager.closeConnection(conn);
		}
	}
	
	public int Delete(int nodeId) throws SQLException, IOException {
		String query = properties.getProperty("comm.node.query.delete");
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, nodeId);
			return pstmt.executeUpdate();
		}
		finally
		{
			dbManager.closeConnection(conn);
		}
	}


	public List<Node> GetAllNodes() throws SQLException, IOException{
		List<Node> nodes = new ArrayList<Node>();
		String query = properties.getProperty("comm.node.query.getAllNodes");
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		Statement statement=conn.createStatement();
		try
		{
			ResultSet rs=statement.executeQuery(query);
			getNodeList(nodes, rs);
		}
		finally
		{
			dbManager.closeConnection(conn);
		}
		return nodes;

	}


	public List<Node> GetAvailableNodes() throws SQLException, IOException {
		// TODO Auto-generated method stub
		List<Node> nodes = new ArrayList<Node>();
		String query = properties.getProperty("comm.node.query.GetAvailableNodes");
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		Statement statement=conn.createStatement();
		ResultSet rs=statement.executeQuery(query);
		try
		{
		rs = statement.executeQuery(query);
			getNodeList(nodes, rs);
			return nodes;
		}
		finally
		{
			dbManager.closeConnection(conn);
		}
   }

	private void getNodeList(List<Node> nodes, ResultSet rs) throws SQLException {
		while (rs.next()) {
			Node processingnode = new Node();
			processingnode.setNodeId(rs.getInt("nodeId"));
			processingnode.setNodePort(rs.getInt("nodePort"));
			processingnode.setNodeIp(rs.getString("nodeIp"));
			nodes.add(processingnode);
		}
	} 
} 