package com.itt.tds.coordinator;

import com.itt.tds.comm.TDSRequest;
import com.itt.tds.comm.TDSResponse;

import java.io.IOException;
import java.sql.SQLException;

public interface TDSController {
    public TDSResponse processRequest(TDSRequest request) throws IOException, SQLException;
    //TprocessRequest
}
