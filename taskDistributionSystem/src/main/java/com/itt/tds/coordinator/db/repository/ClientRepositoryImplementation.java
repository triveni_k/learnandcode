package com.itt.tds.coordinator.db.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import com.itt.tds.client.Client;
import com.itt.tds.coordinator.db.DbManager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class ClientRepositoryImplementation implements ClientRepository{
	Properties properties = new Properties();
	public ClientRepositoryImplementation() throws IOException{
		FileInputStream input = new FileInputStream("src//main//java//com//itt//tds//cfg//config.properties");
    	properties.load(input);
	}
	public int Add(Client client) throws SQLException, IOException {
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try
		{
		String query = properties.getProperty("comm.client.query.add");
		PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, client.getUserName());
        pstmt.setString(2, client.getHostName());
        return pstmt.executeUpdate();
		}
		finally
		{
			dbManager.closeConnection(conn);
		}
	
	 }
	 
	public int Modify(Client client) throws SQLException, IOException {
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try
		{
		String query = properties.getProperty("comm.client.query.update");
		PreparedStatement pstmt = conn.prepareStatement(query);
		pstmt.setString(1, client.getUserName());
		pstmt.setString(2, client.getHostName());
		pstmt.setInt(3, client.getClientId());
		return pstmt.executeUpdate();
		}
		finally
		{
			dbManager.closeConnection(conn);
		}

	}
	public int Delete(int clientId) throws SQLException, IOException {
		
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try
		{
		conn = dbManager.getConnection();
		String query = properties.getProperty("comm.client.query.delete");
		PreparedStatement pstmt = conn.prepareStatement(query);
		pstmt.setInt(1, clientId);
		return pstmt.executeUpdate();
		}
		finally
		{
			dbManager.closeConnection(conn);
		}
	}
	
	public int getClientIdByHostName(String ip) throws SQLException, IOException {
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try {
			String query = "select clientId from client where hostName=?";
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setString(1, ip);
			ResultSet rs = pstmt.executeQuery();
			if(!rs.next()) {
				return -1;
			}
			return rs.getInt("clientId");
		}finally {
			dbManager.closeConnection(conn);
		}
	}

	public List<Client> getClients() throws SQLException, IOException{
		List<Client> clients = new ArrayList<Client>();
	
		DbManager dbManager = DbManager.getInstance(); 
		Connection conn = dbManager.getConnection();
		try
		{
	    String query = properties.getProperty("comm.client.query.getAllClients");
		Statement statement=conn.createStatement();
		ResultSet rs=statement.executeQuery(query);
		while (rs.next()) {
			Client client = new Client();
			client.setHostName(rs.getString("comm.client.hostname"));
			client.setUserName(rs.getString("comm.client.username"));
			clients.add(client);
		}
		return clients;
	}
		finally
		{
			dbManager.closeConnection(conn);
		}
	}
} 