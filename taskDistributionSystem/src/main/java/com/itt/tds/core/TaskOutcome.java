package com.itt.tds.core;

import java.util.*;

/**
 * 
 */
public class TaskOutcome {

    /**
     * Default constructor
     */
    public TaskOutcome() {
    }

    /**
     * 
     */
    public static int SUCCESS = 1;

    /**
     * 
     */
    public static int FAILED = 2;

}