package com.itt.tds.core;

import java.util.*;

/**
 * 
 */
public class TaskState {

    /**
     * Default constructor
     */
    public TaskState() {
    }

    /**
     * 
     */
    public static int PENDING = 1;

    /**
     * 
     */
    public static int IN_PROGRESS = 2;

    /**
     * 
     */
    public static int COMPLETED = 3;

}