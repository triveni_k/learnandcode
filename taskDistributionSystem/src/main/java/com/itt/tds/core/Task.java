package com.itt.tds.core;

import java.util.*;

/**
 * 
 */
public class Task {

    /**
     * Default constructor
     */
    public Task() {
    }
    private byte[] taskData;
    /**
     * 
     */
    private String taskName;

    /**
     * 
     */
    private String taskParameters;

    /**
     * 
     */
    private String taskExePath;

    /**
     * 
     */
    private int taskState;

    /**
     * 
     */
    private TaskResult taskResult;

    /**
     * 
     */
    private int id;

    /**
     * 
     */
    private byte[] programBytes;

    /**
     * 
     */
    private int userId;

    /**
     * 
     */
    private int assignedNodeId;

	private int taskStatus;

    /**
     * @param taskName
     */
	public void setTaskData(byte[] taskData) {
        // TODO implement here
    	this.taskData = taskData;
    }
	public byte[] getTaskData() {
        // TODO implement here
    	return this.taskData;
    }
    public void setTaskName(String taskName) {
        // TODO implement here
    	this.taskName = taskName;
    }

    /**
     * @return
     */
    public String getTaskName() {
        // TODO implement here
        return this.taskName;
    }

    /**
     * @param taskExePath
     */
    public void setTaskExePath(String taskExePath) {
        // TODO implement here
    	this.taskExePath = taskExePath;
    }

    /**
     * @return
     */
    public String getTaskExePath() {
        // TODO implement here
        return this.taskExePath;
    }
    /**
     * @return
     */
    public int getTaskState() {
        // TODO implement here
        return this.taskState;
    }

    /**
     * @param taskResult
     */
    public void setTaskResult(TaskResult taskResult) {
        // TODO implement here
    }

    /**
     * @return
     */
    public TaskResult getTaskResult() {
        // TODO implement here
        return null;
    }

    /**
     * @param id
     */
    public void setId(int id) {
        // TODO implement here
    	this.id = id;
    }

    /**
     * @return
     */
    public int getId() {
        // TODO implement here
        return this.id;
    }

    /**
     * @param taskParamaters
     */
    public void setTaskParameters(String taskParamaters) {
        // TODO implement here
    	this.taskParameters = taskParamaters;
    }

    /**
     * @return
     */
    public String getTaskParameters() {
        // TODO implement here
        return this.taskParameters;
    }

    /**
     * @param userId
     */
    public void setUserId(int userId) {
        // TODO implement here
    	this.userId = userId;
    }

    /**
     * @return
     */
    public int getUserId() {
        // TODO implement here
        return this.userId;
    }

    /**
     * @param bytes
     */
    public void setProgramBytes(byte[] bytes) {
        // TODO implement here
    	this.programBytes = bytes;
    }

    /**
     * @return
     */
    public byte[] getProgramBytes() {
        // TODO implement here
        return this.programBytes;
    }

    /**
     * @return
     */
    public int getAssingedNodeId() {
        // TODO implement here
        return this.assignedNodeId;
    }

    /**
     * @return
     */
    public void setAssignedNodeId(int nodeId) {
        // TODO implement here
        this.assignedNodeId = nodeId;
    }

	public int getTaskStatus() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setTaskStatus(int i) {
		// TODO Auto-generated method stub
		this.taskStatus = i;
		
	}

	public void setTaskState(int i) {
		// TODO Auto-generated method stub
		this.taskState = i;
	}

	

}