package com.itt.tds.comm;
import java.io.*;  

// A Java program for a Client 
import java.net.*;
import java.util.Scanner;
public class Client 
{
	TDSResponse sendRequest(TDSRequest request){      
		try{      
			Socket s=new Socket("localhost",6668);
			sendFile(s, request);
		}catch(Exception e){
			System.out.println(e);
		}
		return null;  
	}    
	private void sendFile(Socket s, TDSRequest request) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("enter 1 to send task or 2 to fetch the result");
		Scanner scanner = new Scanner(System.in);
		String select = scanner.next();
		if(select == "1") {
			System.out.println("enter the task path");
			String path = scanner.next();
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());
			FileInputStream fis = new FileInputStream(path);
			byte[] buffer = new byte[4096];
			request.setData(buffer);
			request.addParameter("task-method", "task-add");
			request.addParameter("userName", System.getProperty("user.name"));
			request.setSourceIp(InetAddress.getLocalHost().toString());
			dos.writeUTF(TDSSerializerFactory.getSerializer().Serialize(request));
			fis.close();
			dos.close();	
		}else {
			System.out.println("enter the task Id");
			String taskId = scanner.next();
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());
			request.addParameter("task-method", "task-get");
			request.addParameter("taskId", taskId);
			dos.writeUTF(TDSSerializerFactory.getSerializer().Serialize(request));
			dos.close();	
		}
	}
	public static void main(String args[]) 
	{ 
		Client client = new Client();
		client.sendRequest(new TDSRequest());
	} 
} 



/*
DataOutputStream dout=new DataOutputStream(s.getOutputStream());
JsonSerializer TdsSerializer = new JsonSerializer();
dout.writeUTF(TdsSerializer.Serialize(request));
InputStream inFromServer = s.getInputStream();
DataInputStream in = new DataInputStream(inFromServer);
String response = in.readUTF(); 
TDSResponse tdsResponse = TDSSerializerFactory.getSerializer().DeSerialize(response, TDSResponse.class);
System.out.println(tdsResponse.getErrorCode());
dout.flush();  
dout.close();  
s.close();



TDSRequest request = new TDSRequest();
		request.setDestIp("192.168.5.40");
		request.setSourcePort(8000);
		request.setProtocolFormat("json");
		request.setSourceIp("192.168.4.57");
		request.setDestPort(8000);
		request.addParameter("node-name", "node1");
		request.addParameter("node-ip", "192.168.4.57");
		request.addParameter("node-port", "8000");
		request.addParameter("node-method", "node-add");
 */