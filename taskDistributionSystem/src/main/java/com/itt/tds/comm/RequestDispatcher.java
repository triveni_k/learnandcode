package com.itt.tds.comm;



import com.itt.tds.coordinator.NodeController;
import com.itt.tds.coordinator.TDSController;
import com.itt.tds.coordinator.TaskController;
//TDSController
public class RequestDispatcher {
    public static TDSController getController(TDSRequest request) {
        if (request.getMethod().startsWith("node-")) {
            return new NodeController();
        } else if (request.getMethod().startsWith("task-")) {
            return new TaskController();
        }else{
            //return nodeController;
            return null;
        }
    }
}
