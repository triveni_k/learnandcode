package com.itt.tds.comm;

import com.itt.tds.coordinator.TDSController;

import java.util.ArrayList;
import java.util.List;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import com.itt.tds.core.Task;

public class SocketHandler extends Thread {
    Socket sock;
    List<Task> task = new ArrayList();
    public SocketHandler() {
    	
    }
    
    public void setTask(Task task) {
    	this.task.add(task);
    }

    SocketHandler(Socket sock) {
        this.sock = sock;
    }

    public void run() {
        try {
            TDSRequest requestData = null;
            requestData = getRequest(sock);
            //Convert the request to TDSRequest
            TDSController controller = RequestDispatcher.getController(requestData);
            TDSResponse response = controller.processRequest(requestData);     
           // response.setParameters("nodeId", "1");
            response.setStatus(true);
            response.setErrorCode(500);
            String str = TDSSerializerFactory.getSerializer().Serialize(response);
            writeResponse(this.sock, str);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

        TDSRequest getRequest (Socket sock) throws IOException {
            DataInputStream input = new DataInputStream(sock.getInputStream());
            String str = (String) input.readUTF();
            JsonSerializer TdsSerializer = new JsonSerializer();
            TDSRequest tdsRequest = new TDSRequest();
            tdsRequest = TdsSerializer.DeSerialize(str, tdsRequest.getClass());
            return tdsRequest;
        }

        private void writeResponse (Socket sock, String responseData) throws IOException{
        	System.out.println(responseData);
        	DataOutputStream out = new DataOutputStream(sock.getOutputStream());
    		out.writeUTF(responseData);
        }
        public static void main (String[]args) throws IOException {
            ServerSocket ssock = new ServerSocket(6668);
            try {
                System.out.println("Listening");

                while (true) {
                    Socket sock = ssock.accept();
                    new SocketHandler(sock).start();
                }
            } finally {
                ssock.close();
            }
        }
    }
