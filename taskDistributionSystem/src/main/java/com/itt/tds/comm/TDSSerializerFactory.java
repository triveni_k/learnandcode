
package com.itt.tds.comm;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class TDSSerializerFactory {
	static Properties properties = new Properties();

	TDSSerializerFactory() throws IOException{
		FileInputStream input = new FileInputStream("src//main//java//com//itt//tds//cfg//config.properties");
    	Properties properties = new Properties();
    	properties.load(input);
	}

	public static ITdsSerializer getSerializer() {
		return new JsonSerializer();
	}
	public static void main(String args[]) throws IOException {
		TDSSerializerFactory ts = new TDSSerializerFactory();
		String x = properties.getProperty("tdsProtocol.Format");
		System.out.println(ts.getSerializer());
	}

}

