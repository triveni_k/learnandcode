package com.itt.tds.comm;

import java.util.*;

/**
 * 
 */
public class TDSProtocol {

    /**
     * Default constructor
     */
    public TDSProtocol() {
    }
    
    /**
     * 
     */
    public String sourceIp;

    /**
     * 
     */
    public String destIp;

    /**
     * 
     */
    public int sourcePort;

    /**
     * 
     */
    public int destPort;

    /**
     * 
     */
    public String protocolFormat;

    /**
     * 
     */
    public String protocolVersion;
    public String data;
    /**
     * 
     */
    public Hashtable headers = new Hashtable<String, String>() ;

	public String getSourceIp() {
		return sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	public String getDestIp() {
		return destIp;
	}

	public void setDestIp(String destIp) {
		this.destIp = destIp;
	}

	public int getSourcePort() {
		return sourcePort;
	}

	public void setSourcePort(int sourcePort) {
		this.sourcePort = sourcePort;
	}

	public int getDestPort() {
		return destPort;
	}

	public void setDestPort(int destPort) {
		this.destPort = destPort;
	}

	public String getProtocolFormat() {
		return protocolFormat;
	}

	public void setProtocolFormat(String protocolFormat) {
		this.protocolFormat = protocolFormat;
	}

	public String getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public Hashtable getHeaders() {
		return headers;
	}

	public void setHeaders(Hashtable headers) {
		this.headers = headers;
	}



}