package com.itt.tds.comm;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class XmlSerializer implements ITdsSerializer {
	public String  Serialize(TDSProtocol request) throws JsonProcessingException {
		 XmlMapper xmlMapper = new XmlMapper();
		    String xml = xmlMapper.writeValueAsString(request);
		return xml;
	}

    public <T> T DeSerialize(String data,Class <T> test) throws JsonParseException, JsonMappingException, IOException
	{
	XmlMapper xmlMapper = new XmlMapper();
	T protocol = xmlMapper.readValue(data, test);
        return (T) protocol;
	}
}//<TDSRequest><sourceIp>192.168.4.57</sourceIp><destIp>192.168.5.40</destIp><sourcePort>8000</sourcePort><destPort>8000</destPort><protocolFormat>json</protocolFormat><protocolVersion/><headers><node-ip>192.168.4.57</node-ip><node-port>8000</node-port><node-name>node1</node-name></headers></TDSRequest>

