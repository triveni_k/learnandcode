package com.itt.tds.comm;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.*;

import org.json.JSONObject;

/**
 *
 */
public class JsonSerializer implements ITdsSerializer{

	ObjectMapper mapper = new ObjectMapper();

    /**
     * @param protocolObect
     * @param rawData
     * @throws JsonProcessingException
     */
    public String  Serialize(TDSProtocol request) throws JsonProcessingException {
    	return this.mapper.writeValueAsString(request);
    }

    public <T> T DeSerialize(String data,Class <T> test) throws JsonParseException, JsonMappingException, IOException {
        T protocol = mapper.readValue(data,test);
        System.out.println("#######"+ protocol);
        return (T) protocol;
	}
}
