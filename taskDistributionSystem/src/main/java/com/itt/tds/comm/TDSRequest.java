package com.itt.tds.comm;

import java.util.*;

/**
 * 
 */
public class TDSRequest extends TDSProtocol {
	public byte[] data;

    /**
     * Default constructor
     */
    public TDSRequest() {
    }

    /**
     * 
     */
    /*public void getMethod() {

    }*/
    public void setData(byte[] data) {
        this.data=data;
     }
    /**
     * 
     */
    public String getParameters(String key) {
       return (String) this.headers.get(key);
    }

    /**
     * 
     */
    public byte[] getData() {
    	return data;
    }


    /**
     * @param method
     */
    public void setMethod(String method) {
        // TODO implement here
    }

    /**
     * @param params
     */
    public void setParameters(Hashtable params) {
        // TODO implement here
    }

    public void addParameter(String key,String value) {
      Hashtable<String, String> addParameterData = getHeaders();
      addParameterData.put(key, value);
    }
    public String getMethod() {
        return (String) this.headers.get("task-method");
    }

	
  } 