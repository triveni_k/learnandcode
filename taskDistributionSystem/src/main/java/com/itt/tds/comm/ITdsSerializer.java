package com.itt.tds.comm;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface ITdsSerializer {
	String  Serialize(TDSProtocol request) throws JsonProcessingException;
	public <T> T DeSerialize(String data,Class <T> test) throws JsonParseException, JsonMappingException, IOException;
}
