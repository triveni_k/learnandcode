package com.itt.tds.cfg;

import java.io.FileInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
public class TDSConfiguration {
	public static TDSConfiguration tdsConfig;

    public TDSConfiguration() throws IOException {
    }
    public static synchronized TDSConfiguration getInstance() throws IOException {
    	 if (tdsConfig == null) {
    		 tdsConfig = new TDSConfiguration();
    	    }
    	    return tdsConfig;
    }
    public String getDBConnectionString() throws IOException {
    	String url;
    	String driver;
    	String username;
    	String pwd;
    	FileInputStream input = new FileInputStream("src//main//java//com//itt//tds//cfg//config.properties");
    	Properties properties = new Properties();
		// load a properties file
    	properties.load(input);
    	url = properties.getProperty("jdbc.url");
    	driver = properties.getProperty("jdbc.driver");
    	username = properties.getProperty("jdbc.username");
    	pwd = properties.getProperty("jdbc.pwd");
        url = url+"user="+username+"&password="+pwd;
        return url;
    }
    
}
