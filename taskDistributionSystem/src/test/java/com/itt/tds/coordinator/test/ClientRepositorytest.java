/*
package com.itt.tds.coordinator.test;

import junit.framework.TestCase;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.itt.tds.client.Client;
import com.itt.tds.coordinator.db.repository.ClientRepositoryImplementation;


public class ClientRepositoryTest extends TestCase {

	@Test
	public void testAddClient() throws SQLException, IOException {
		// Arrange

		ClientRepositoryImplementation clientRepoObj = new ClientRepositoryImplementation();
		Client client = new Client();
		int result = 0;
		client.setUserName("triveni");
		client.setHostName("triveni.k");

		// Act
		result = clientRepoObj.Add(client);
		// Assert
		assertEquals(true, result > 0);

	}

	@Test
	public void testModifyClient() throws SQLException, IOException {
		// Arrange
		ClientRepositoryImplementation clientRepoObj = new ClientRepositoryImplementation();
		List<Client> clientList = new ArrayList<Client>();
		Client client = new Client();
		client.setHostName("tri");
		client.setUserName("6789");
		client.setClientId(2);

		// Act

		int result = clientRepoObj.Modify(client);
		// Assert
		assertEquals(true, result == 1);

	}

	@Test
	public void negTestModifyClient() throws IOException {
		// Arrange
		ClientRepositoryImplementation clientRepoObj = new ClientRepositoryImplementation();
		List<Client> clientList = new ArrayList<Client>();
		Client client = new Client();
		client.setHostName("triveni");
		client.setUserName("k");
		client.setClientId(2);

		// Act

		int result = 0;
		try {
			result = clientRepoObj.Modify(client);
		} catch (Exception e) {
			result = 0;
		}
		// Assert
		assertEquals(0, result);

	}

	@Test
	public void testDeleteClients() throws Exception {
		// Arrange

		ClientRepositoryImplementation clientRepoObj = new ClientRepositoryImplementation();
		int id = 3;
		// Act&&Assert
		assertEquals(true, clientRepoObj.Delete(id) == 90);

	}

	@Test
	public void testGetClients() throws Exception {
		// Arrange

		ClientRepositoryImplementation clientRepoObj = new ClientRepositoryImplementation();
		List<Client> clientList = new ArrayList<Client>();
		// Act
		clientList = clientRepoObj.getClients();
		// Assert

		assertEquals(true, clientList.size() > 0);
	}

}*/
