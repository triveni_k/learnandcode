package com.itt.tds.coordinator.test;

import java.io.IOException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import com.itt.tds.coordinator.db.repository.NodeRepository;

import junit.framework.TestCase;
import com.itt.tds.coordinator.Node;
import com.itt.tds.coordinator.db.repository.NodeRepositoryImplementation;

public class NodeRepositoryTest extends TestCase {
	static int id;
	@Test
	public void testAdd() throws SQLException, IOException {
		//Arrange
		NodeRepositoryImplementation nodeImpl = new NodeRepositoryImplementation();
		Node node = new Node();
		int result = 0;
		Random ran = new Random();
	    node.setNodeIp(Integer.toString(ran.nextInt(100)));
	    node.setNodePort(123456789);
	    node.setNodeStatus(1);
		//Act
		result = nodeImpl.Add(node);
		//Assert
		 assertEquals(1,result);
	}
	@Test
	public void testGetAvailableNodes() throws SQLException, IOException{
		//Arrange
		NodeRepositoryImplementation nodeImpl = new NodeRepositoryImplementation();	
		List<Node> node = new ArrayList<Node>();
		//Act
		node = nodeImpl.GetAvailableNodes();
		getId(node);
		//Assert
		assertEquals(true,node.size()>0);
	}

	private void getId(List<Node> node) {
		for (int i = 0; i < node.size(); i++) {
			Node lnode = node.get(i);
			if(lnode.getNodePort() == 123456789) {
				this.id = lnode.getNodeId();
			}
		}
	}
	@Test
	public void testModify() throws SQLException, IOException {
		//Arrange
		NodeRepositoryImplementation nodeImpl = new NodeRepositoryImplementation();
		Node node = new Node();
		int result = 0;
	    node.setNodeId(this.id);
	    node.setNodeStatus(2);
		//Act
		result = nodeImpl.Modify(node);
		System.out.print(result);
		//Assert
		assertEquals(0,result);
	}
	@Test
	public void testNegModify() throws IOException {
		//Arrange
		NodeRepositoryImplementation nodeImpl = new NodeRepositoryImplementation();
		Node node = new Node();
		int result = 0;
	    node.setNodeStatus(1);
		node.setNodeId(2);
		try{
			//Act
		result = nodeImpl.Modify(node);
		}
		catch (Exception e) {
			// TODO: handle exception
			result = 0;
		}
		
	//Assert
		assertEquals(1,result);
	}
	@Test
	public void testDelete() throws SQLException, IOException{
		//Arrange
		int result;
		NodeRepositoryImplementation nodeImpl = new NodeRepositoryImplementation();
		//Act
		result = nodeImpl.Delete(this.id);
		//Assert
		assertEquals(1,result);
	}
	@Test
	public void testNegDelete() throws Exception, IOException{
		//Arrange
		int result = 0, id = 1;
		NodeRepositoryImplementation nodeImpl = new NodeRepositoryImplementation();
		
			//Act
		result = nodeImpl.Delete(id);
		
			// TODO: handle exception
			result = 0;
			
		
	
		//Assert
		assertEquals(0,result);
	}
} 