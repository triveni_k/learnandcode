/*
package com.itt.tds.comm.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

import com.itt.tds.comm.TDSRequest;
import com.itt.tds.comm.XmlSerializer;

public class xmlSerializer {
	@Test
	public void testSerialize() throws SQLException, IOException {
		XmlSerializer ser = new XmlSerializer();
		TDSRequest request = new TDSRequest();
		createRequestIns(request);
		String xml = "<TDSRequest><sourceIp>192.168.4.57</sourceIp><destIp>192.168.5.40</destIp><sourcePort>8000</sourcePort><destPort>8000</destPort><protocolFormat>xml</protocolFormat><protocolVersion/><headers><node-ip>192.168.4.57</node-ip><node-port>8000</node-port><node-name>node1</node-name></headers></TDSRequest>";
		assertEquals(xml, ser.Serialize(request));
	}
	private void createRequestIns(TDSRequest request) {
		request.setDestIp("192.168.5.40");
		request.setSourcePort(8000);
		request.setProtocolFormat("xml");
		request.setSourceIp("192.168.4.57");
		request.setDestPort(8000);
		request.addParameter("node-name", "node1");
		request.addParameter("node-ip", "192.168.4.57");
		request.addParameter("node-port", "8000");
	}
	@Test
	public void negTestSerialize() throws SQLException, IOException {
		System.out.println("2345678");
		XmlSerializer ser = new XmlSerializer();
		String xml = "<TDSRequest><sourceIp>192.168.4.57</sourceIp><destIp>192.168.5.40</destIp><sourcePort>8000</sourcePort><destPort>8000</destPort><protocolFormat>xml</protocolFormat><protocolVersion/><headers><node-ip>192.168.4.57</node-ip><node-port>8000</node-port><node-name>node1</node-name></headers></TDSRequest>";
		//TDSRequest request = ser.DeSerialize(xml);
		assertEquals(request.getDestIp(), "192.168.5.40");
		assertEquals(request.getDestPort(), 8000);
		assertEquals(request.getProtocolFormat(), "xml");
		assertEquals(request.getSourceIp(), "192.168.4.57");
	}

}

*/
