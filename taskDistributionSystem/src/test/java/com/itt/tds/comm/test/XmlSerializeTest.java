package com.itt.tds.comm.test;
import com.itt.tds.comm.TDSResponse;
import junit.framework.TestCase;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;

import org.json.JSONObject;
import org.junit.Test;

import com.itt.tds.comm.TDSProtocol;
import com.itt.tds.comm.TDSRequest;
import com.itt.tds.comm.JsonSerializer;

public class XmlSerializeTest {
	static String reqJson;	
	
	@Test
	public void testSerializeRequest() throws SQLException, IOException {
		JsonSerializer tdsSerialize = new JsonSerializer();
		TDSRequest request = new TDSRequest();
		createRequestIns(request);
		this.reqJson = tdsSerialize.Serialize(request);
		JSONObject jsonObj = new JSONObject(reqJson);
		assertEquals(jsonObj.getString("destIp"), "192.168.5.40");
		assertEquals(jsonObj.getString("sourceIp"), "192.168.4.57");
		assertEquals(jsonObj.getInt("sourcePort"), 8000);
		assertEquals(jsonObj.getJSONObject("headers").getString("node-method"), "node-add");
		System.out.println(jsonObj);
	}
	private void createRequestIns(TDSRequest request) {
		request.setDestIp("192.168.5.40");
		request.setSourcePort(8000);
		request.setProtocolFormat("json");
		request.setSourceIp("192.168.4.57");
		request.setDestPort(8000);
		request.addParameter("node-name", "node1");
		request.addParameter("node-ip", "192.168.4.57");
		request.addParameter("node-port", "8000");
		request.addParameter("node-method", "node-add");
	}
	@Test
	public void testDeSerializeResponse() throws SQLException, IOException {
		JsonSerializer tdsSerialize = new JsonSerializer();
		TDSResponse response = new TDSResponse();
		response =tdsSerialize.DeSerialize("{\"sourceIp\":\"192.168.4.57\",\"destIp\":\"192.168.5.40\",\"sourcePort\":8000,\"destPort\":8000,\"protocolFormat\":\"json\",\"protocolVersion\":null,\"headers\":{\"node-ip\":\"192.168.4.57\",\"node-port\":\"8000\",\"node-name\":\"node1\"}}\n", response.getClass());
		assertEquals(response.getDestIp(), "192.168.5.40");
		assertEquals(response.getDestPort(), 8000);
		assertEquals(response.getProtocolFormat(), "json");
		assertEquals(response.getSourceIp(), "192.168.4.57");

	}
	private void createRequestIns(TDSResponse res) {
		res.setDestIp("192.168.5.40");
		res.setSourcePort(8000);
		res.setProtocolFormat("json");
		res.setSourceIp("192.168.4.57");
		res.setDestPort(8000);
		res.addParameter("node-name", "node1");
		res.addParameter("node-ip", "192.168.4.57");
		res.addParameter("node-port", "8000");
	}
	@Test
	public void testSerializeRes() throws SQLException, IOException {
		JsonSerializer tdsSerialize = new JsonSerializer();
		TDSResponse response = new TDSResponse();
		createRequestIns(response);
		this.reqJson = tdsSerialize.Serialize(response);
		JSONObject jsonObj = new JSONObject(reqJson);
		assertEquals(jsonObj.getString("destIp"), "192.168.5.40");
		assertEquals(jsonObj.getString("sourceIp"), "192.168.4.57");
		assertEquals(jsonObj.getInt("sourcePort"), 8000);
		assertEquals(jsonObj.getInt("destPort"), 8000);
	}
	@Test
	public void testDeSerializeReq() throws SQLException, IOException {
		JsonSerializer tdsSerialize = new JsonSerializer();
		TDSRequest response = new TDSRequest();
		response =tdsSerialize.DeSerialize("{\"destIp\":\"192.168.5.40\",\"headers\":{\"node-method\":\"node-add\",\"node-ip\":\"192.168.4.57\",\"node-name\":\"node1\",\"node-port\":\"8000\"},\"sourcePort\":8000,\"destPort\":8000,\"sourceIp\":\"192.168.4.57\",\"method\":\"node-add\",\"protocolVersion\":null,\"protocolFormat\":\"json\"}\n", response.getClass());
		assertEquals(response.getDestIp(), "192.168.5.40");
		assertEquals(response.getDestPort(), 8000);
		assertEquals(response.getProtocolFormat(), "json");
		assertEquals(response.getSourceIp(), "192.168.4.57");
		assertEquals(response.getMethod(), "node-add");

	}
}
